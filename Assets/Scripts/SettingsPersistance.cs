﻿using System;

[Serializable]
public class SettingsPersistance
{
    public bool bMusic = true;
    public bool bDirectionByRotation = true;
    public int iDifficulty = 1;
    public int iSensibility = 2;
    public int iScene = 0;
    public bool bDistanceDuration = true;

}
