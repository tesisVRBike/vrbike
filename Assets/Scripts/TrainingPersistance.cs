﻿using System;
[Serializable]
public class TrainingPersistance
{
    public int iDifficulty = 0;
    public float fMeters = 0;
    public float fTime = 0; // Time in seconds
}
