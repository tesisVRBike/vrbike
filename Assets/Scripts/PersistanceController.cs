﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistanceController : MonoBehaviour {
    public SettingsPersistance settings = new SettingsPersistance();
    public TrainingTablePersistance trainings = new TrainingTablePersistance();
	// Use this for initialization
	void Awake () {
        settings = JsonUtility.FromJson<SettingsPersistance>(PlayerPrefs.GetString(VRBConstants.KEY_SETTINGS,"{}"));
        trainings = JsonUtility.FromJson<TrainingTablePersistance>(PlayerPrefs.GetString(VRBConstants.KEY_TRAININGS, "{\"TablePersistance\":[]}"));
    }
	
	public void SaveSettings()
    {
        PlayerPrefs.SetString(VRBConstants.KEY_SETTINGS, JsonUtility.ToJson(settings));
    }

    public void SaveTraining()
    {
        PlayerPrefs.SetString(VRBConstants.KEY_TRAININGS, JsonUtility.ToJson(trainings));
    }
}
