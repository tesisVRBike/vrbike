﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {
    public GameObject goMainMenu;
    public GameObject goPauseMenu;
    public GameObject goMusic;
    public GameObject goDirection;
    public GameObject goDifficulty;
    public GameObject goStage;
    public GameObject goSensibility;
    public GameObject goDurationType;
    public GameObject[] goTableTraining;
    public AudioSource asAudio;
    public Image imgFill;

    private float fInitialTime = 0f;
    private float fActualTime = 0f;
    public float fTimeDurationPause = 0f;
    public float fDistancePause = 0f;
    private bool bLooking;
    private bool bMusicOn = true;
    private bool bDirectionByRotation = true;
    private bool bDistanceDuration = true;
    private string sToCallFunction = "";
    private int iDifficulty = 1;
    private int iStage = 0;
    private int iSensibility = 0;

    // Use this for initialization
    void Start () {
        SetMusic(GetComponent<PersistanceController>().settings.bMusic);
        SetDirection(GetComponent<PersistanceController>().settings.bDirectionByRotation);
        SetDifficulty(GetComponent<PersistanceController>().settings.iDifficulty);
        SetSensibility(GetComponent<PersistanceController>().settings.iSensibility);
        SetStage(GetComponent<PersistanceController>().settings.iScene);
        SetDurationType(GetComponent<PersistanceController>().settings.bDistanceDuration);
        FillTrainingTable();

    }
	
	// Update is called once per frame
	void Update () {
        if (bLooking)
        {
            imgFill.fillAmount = (Time.time - fInitialTime) / VRBConstants.GAZE_INPUT_TIME;
            if (imgFill.fillAmount >= 1)
            {
                bLooking = false;
                imgFill.fillAmount = 0;
                StartCoroutine(sToCallFunction);
            }
        }
	}

    public void OnStartLooking(string sCallback)
    {
        sToCallFunction = sCallback;
        fInitialTime = Time.time;
        bLooking = true;

    }

    public void OnExitLooking()
    {
        bLooking = false;
        imgFill.fillAmount = 0;
    }

    public IEnumerator StartTraining()
    {
        yield return new WaitForSeconds(0);
        SetTraining(true);
        GetComponent<Autowalk>().bPause = false;
        HideMainMenu();
        HidePauseMenu();
    }

    public void StopTraining(float fDistance, float fTimeDuration)
    {
        Debug.Log("Metros recorridos: " + fDistance);
        Debug.Log("Tiempo transcurrido: " + fTimeDuration);
        SetTraining(false);
        GetComponent<Autowalk>().bPause = false;
        GetComponent<Autowalk>().fTotalMeters = 0f;
        GetComponent<Autowalk>().fTimeTrainingSeconds = 0f;
        this.transform.localPosition = VRBConstants.INITIAL_POSITION;
        HidePauseMenu();
        ShowMainMenu();
        SaveTraining(fDistance, fTimeDuration);
        FillTrainingTable();
    }

    private void SaveTraining(float fDistance, float fTime)
    {
        TrainingPersistance newTR = new TrainingPersistance();
        newTR.fMeters = fDistance;
        newTR.iDifficulty = GetComponent<PersistanceController>().settings.iDifficulty;
        newTR.fTime = fTime;

        GetComponent<PersistanceController>().trainings.TablePersistance.Add(newTR);

        GetComponent<PersistanceController>().SaveTraining();
    }

    private void SetTraining(bool bEnabled)
    {
        GetComponent<Autowalk>().enabled = bEnabled;
        GetComponent<CharacterMotor>().enabled = bEnabled;
    }

    private void SetMainMenu(bool bEnabled)
    {
        goMainMenu.SetActive(bEnabled);
    }

    private void ShowMainMenu()
    {
        SetMainMenu(true);
    }

    private void HideMainMenu()
    {
        SetMainMenu(false);
    }

    public void PauseTraining(Vector3 v3HeadPosition, float fDistance, float fTimeDuration)
    {
        fDistancePause = fDistance;
        fTimeDurationPause = fTimeDuration;
        SetTraining(false);
        ShowPauseMenu(v3HeadPosition);
    }
    private void ShowPauseMenu(Vector3 v3PauseMenuPosition)
    {
        goPauseMenu.transform.position = v3PauseMenuPosition + VRBConstants.PAUSE_MENU_OFFSET;
        SetPauseMenu(true);
    }

    private void HidePauseMenu()
    {
        SetPauseMenu(false);
    }

    private void SetPauseMenu(bool bEnabled)
    {
        goPauseMenu.SetActive(bEnabled);
    }
    
    public IEnumerator ChangeMusic()
    {
        yield return new WaitForSeconds(0);
        bMusicOn = !GetComponent<PersistanceController>().settings.bMusic;
        SetMusic(bMusicOn);
    }
    
    private void SetMusic(bool bEnabled)
    {
        if (bEnabled)
        {
            asAudio.mute = false;
            goMusic.GetComponent<Text>().text = "Sonido: ON";
        }
        else
        {
            asAudio.mute = true;
            goMusic.GetComponent<Text>().text = "Sonido: OFF";
        }
        GetComponent<PersistanceController>().settings.bMusic = bEnabled;
        GetComponent<PersistanceController>().SaveSettings();
    }

    public IEnumerator ChangeDirection()
    {
        yield return new WaitForSeconds(0);
        bDirectionByRotation = !GetComponent<PersistanceController>().settings.bDirectionByRotation;
        SetDirection(bDirectionByRotation);
    }

    private void SetDirection(bool bEnabled)
    {
        if (bEnabled)
        {
            goDirection.GetComponent<Text>().text = "Direccion: Por Rotacion";
        }
        else
        {
            goDirection.GetComponent<Text>().text = "Direccion: Fijo";
        }
        GetComponent<PersistanceController>().settings.bDirectionByRotation = bEnabled;
        GetComponent<PersistanceController>().SaveSettings();
    }

    public IEnumerator ChangeDifficulty()
    {
        yield return new WaitForSeconds(0);
        iDifficulty = GetComponent<PersistanceController>().settings.iDifficulty;
        if (iDifficulty == 2)
        {
            iDifficulty = 0;
        }
        else
        {
            iDifficulty++;
        }
        SetDifficulty(iDifficulty);
    }

    private void SetDifficulty(int iLevel)
    {
        switch (iLevel)
        {
            case 0:
                goDifficulty.GetComponent<Text>().text = "Dificultad: "+ VRBConstants.LANG_DIFFICULTY_LOW;
                break;
            case 1:
                goDifficulty.GetComponent<Text>().text = "Dificultad: " + VRBConstants.LANG_DIFFICULTY_MID;
                break;
            case 2:
                goDifficulty.GetComponent<Text>().text = "Dificultad: " + VRBConstants.LANG_DIFFICULTY_HIGH;
                break;
        }
        GetComponent<PersistanceController>().settings.iDifficulty = iLevel;
        GetComponent<PersistanceController>().SaveSettings();
    }

    public IEnumerator ChangeStage()
    {
        yield return new WaitForSeconds(0);
        iStage = GetComponent<PersistanceController>().settings.iScene;
        if (iStage == 1)
        {
            iStage = 0;
        }
        else
        {
            iStage++;
        }
        SetStage(iStage);
    }

    private void SetStage(int iLevel)
    {

        goStage.GetComponent<Text>().text = ScenarioToString(iLevel);

        GetComponent<PersistanceController>().settings.iScene = iLevel;
        GetComponent<BackgroundPool>().FillPoolTerrain(iLevel);
        GetComponent<PersistanceController>().SaveSettings();

    }

    public IEnumerator ChangeSensibility()
    {
        yield return new WaitForSeconds(0);
        iSensibility = GetComponent<PersistanceController>().settings.iSensibility;
        if (iSensibility == 4)
        {
            iSensibility = 0;
        }
        else
        {
            iSensibility++;
        }
        SetSensibility(iSensibility);
    }

    private void SetSensibility(int iNewSensibility)
    {
        /*
        switch (iLevel)
        {
            case 0:
                goSensibility.GetComponent<Text>().text = "Sensibilidad: 1";
                break;
            case 1:
                goSensibility.GetComponent<Text>().text = "Sensibilidad: 2";
                break;
            case 2:
                goSensibility.GetComponent<Text>().text = "Sensibilidad: 3";
                break;
            case 3:
                goSensibility.GetComponent<Text>().text = "Sensibilidad: 4";
                break;
            case 4:
                goSensibility.GetComponent<Text>().text = "Sensibilidad: 5";
                break;
        }*/

        goSensibility.GetComponent<Text>().text = VRBConstants.LANG_SENSIBILITY + iNewSensibility;
        GetComponent<PersistanceController>().settings.iSensibility = iNewSensibility;
        GetComponent<PersistanceController>().SaveSettings();
    }

    public IEnumerator ChangeDurationType()
    {
        yield return new WaitForSeconds(0);
        bDistanceDuration = !GetComponent<PersistanceController>().settings.bDistanceDuration;
        SetDurationType(bDistanceDuration);
    }

    private void SetDurationType(bool bDuration)
    {
        if (bDuration)
        {
            goDurationType.GetComponent<Text>().text = "Duracion: Distancia";
        }
        else
        {
            goDurationType.GetComponent<Text>().text = "Duracion: Tiempo";
        }
        GetComponent<PersistanceController>().settings.bDistanceDuration = bDuration;
        GetComponent<PersistanceController>().SaveSettings();
    }

    public IEnumerator Resume()
    {
        yield return new WaitForSeconds(0);
        GetComponent<Autowalk>().bPause = false;
        StartCoroutine(StartTraining());
    }

    public IEnumerator Exit()
    {
        yield return new WaitForSeconds(0);
        StopTraining(fDistancePause,fTimeDurationPause);
    }

    private void FillTrainingTable()
    {
        int iNumberOfTrainings = 0;
        for (int i = GetComponent<PersistanceController>().trainings.TablePersistance.Count-1; i >= 0 && iNumberOfTrainings <= VRBConstants.NUMBER_OF_SAVED_TRAININGS -1; i-- )
        {
            goTableTraining[iNumberOfTrainings].GetComponentsInChildren<Text>()[VRBConstants.TABLE_TRAINING_TIME].text = Mathf.Floor(GetComponent<PersistanceController>().trainings.TablePersistance[i].fTime / 60).ToString("00") + ":" + (GetComponent<PersistanceController>().trainings.TablePersistance[i].fTime % 60).ToString("00");
            goTableTraining[iNumberOfTrainings].GetComponentsInChildren<Text>()[VRBConstants.TABLE_TRAINING_DIFFICULTY].text = DifficultyToString(GetComponent<PersistanceController>().trainings.TablePersistance[i].iDifficulty);
            goTableTraining[iNumberOfTrainings].GetComponentsInChildren<Text>()[VRBConstants.TABLE_TRAINING_METERS].text = GetComponent<PersistanceController>().trainings.TablePersistance[i].fMeters.ToString("F0");
            iNumberOfTrainings++;
        }

    }

    private string DifficultyToString(int iDifficulty)
    {
        string sRes = VRBConstants.LANG_DIFFICULTY_LOW;
        switch (iDifficulty)
        {
            case 1:
                sRes = VRBConstants.LANG_DIFFICULTY_MID;
                break;
            case 2:
                sRes = VRBConstants.LANG_DIFFICULTY_HIGH;
                break;
        }
        return sRes;

    }

    private string ScenarioToString(int iScenario)
    {
        string sRes = VRBConstants.LANG_SCENARIO_FOREST;
        if (iScenario == 1)
        {
            sRes = VRBConstants.LANG_SCENARIO_BEACH;
        }
        return sRes;

    }
}
