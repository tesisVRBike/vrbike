﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Autowalk : MonoBehaviour
{
    //Classes
    private CardboardHead CHead;
    public CharacterMotor.CharacterMotorMovement CMMovement;
    private FPSInputController autowalk;
    //Vectors
    private Vector3 v3LastCheckedPosition;
    //Floats
    private float fDelay            = 0.0f;
    private float fPrevGyroZ        = 0.0f;
    private float fGyroZ            = 0.0f;
    private float fGyroZ1           = 0.0f;
    private float fGyroZ2           = 0.0f;
    private float fDistance         = 0.0f;
    private float fTime             = 0.0f;
    private float fSpeed            = 0.0f;
    private float fFinalX           = 0.0f;
    private float fInitialX         = 0.0f;
    private float fFinalTime        = 0.0f;
    private float fInitialTime      = 0.0f;
    private float fPreviousAngle    = 0.0f;
    private float fCurrentAngle     = 0.0f;

    private float fCurrentAccelerationX = 0.0f;
    public float fTotalMeters = 0.0f;
    private float fAccelerationValueStoredForTime = 0.0f;
    private float fTimeSinceLastStep = VRBConstants.MAX_TIME_BETWEEN_ACCELERATION;
    private float fPreviousTimeStamp = 0.0f;
    public float fTimeTrainingSeconds = 0.0f;
    public float fMetersReverse = 0.0f;
    public float fTimeReverse = 0.0f;

    // private float fPreviousAccelerationX = 0.0f;
    //Constatns
    private const int RADIUS = 2;
    //Texts
    private Text txtSpeed;
    private Text txtMeters;
    private Text txtTime;
    //Booleans
    private bool bIncreasing;
    private bool bDecreasing;
    private bool bOneSideActivated = false;
    private bool bIsRunning = false;
    //Quaternions
    private Quaternion qPrevGyro;
    private Quaternion qCurrentGyro;
    private Quaternion qInitialGyro;
    private Quaternion qPreviousGyro;

    public bool bPause = false;
    private float fMaxVelocityAchieved = 0f;
    private float fCalculatedSpeed = 0f;
    private void Start()
    {
        autowalk = GetComponent<FPSInputController>();

        CHead = Camera.main.GetComponent<StereoController>().Head;
        v3LastCheckedPosition = transform.position;
        txtMeters = GameObject.FindGameObjectWithTag(VRBConstants.TAG_CANVAS).GetComponentsInChildren<Text>()[0];
        txtSpeed = GameObject.FindGameObjectWithTag(VRBConstants.TAG_CANVAS).GetComponentsInChildren<Text>()[1];
        txtTime = GameObject.FindGameObjectWithTag(VRBConstants.TAG_CANVAS).GetComponentsInChildren<Text>()[2];
        qInitialGyro = CHead.transform.rotation;
        fInitialTime = Time.time;
        CMMovement = new CharacterMotor.CharacterMotorMovement();
        fPreviousTimeStamp = Time.realtimeSinceStartup;
    }

    private void Update()
    {
        //New Aproach
        fTimeSinceLastStep = Time.realtimeSinceStartup - fPreviousTimeStamp;
        fCurrentAccelerationX = Mathf.Lerp(fCurrentAccelerationX,
			Input.gyro.userAcceleration.x

            /*+ Input.gyro.userAcceleration.y * VRBConstants.ACCELEROMETER_FILTER_NOT_IN_X 
            + Input.gyro.userAcceleration.z * VRBConstants.ACCELEROMETER_FILTER_NOT_IN_X*/, Time.deltaTime * VRBConstants.ACCELERATION_INPUT_LERP_SPEED);

        fCurrentAccelerationX = Mathf.Round(fCurrentAccelerationX * 100f) / 100f;

        if (Mathf.Abs(fCurrentAccelerationX) > VRBConstants.MIN_ACCELERATION_COUNT)
        {
            if (!bOneSideActivated)
            {
                //txtAccelerometer.text = "ONE SIDED";
                fAccelerationValueStoredForTime = fCurrentAccelerationX;
                bOneSideActivated = true;
            }
            else if (!bIsRunning && bOneSideActivated)
            {
                fPreviousTimeStamp = Time.realtimeSinceStartup;
                if (
                    (fAccelerationValueStoredForTime > 0 && fCurrentAccelerationX < 0)
                    || (fAccelerationValueStoredForTime < 0 && fCurrentAccelerationX > 0))
                {
                    bIsRunning = true;
                }
            }
            else if (bIsRunning)
            {
                fPreviousTimeStamp = Time.realtimeSinceStartup;
                fTimeTrainingSeconds += Time.deltaTime;
                fCalculatedSpeed = Mathf.Abs(fCurrentAccelerationX) * (VRBConstants.SPEED_FACTOR + VRBConstants.SPPED_FACTOR_SENSIBILITY_MODIFIER * GetComponent<PersistanceController>().settings.iSensibility);
               // fMaxVelocityAchieved = Mathf.Max(fCalculatedSpeed, fMaxVelocityAchieved);
                CMMovement.velocity = Vector3.one * fCalculatedSpeed;
                autowalk.checkAutoWalk = true;
            }
            txtSpeed.text = "V: " + CMMovement.velocity.x;
                
        }
        else
        {
            //fMaxVelocityAchieved = 0;
            if (fTimeSinceLastStep > VRBConstants.MAX_TIME_BETWEEN_ACCELERATION)
            {
                bIsRunning = false;
                bOneSideActivated = false;
                txtSpeed.text = "STOPPED";
                autowalk.checkAutoWalk = false;
                if ((!bPause) && (fTimeSinceLastStep > VRBConstants.MAX_TIME_UNTIL_PAUSE))
                {
                    bPause = true;
                    Debug.Log("fTimeTrainingSeconds: " + fTimeTrainingSeconds);
                    GetComponent<MenuController>().PauseTraining(transform.position, fTotalMeters, fTimeTrainingSeconds);
                }
            }
        }
        //Metros de unity llevados a metros reales
        if (Vector3.Distance(v3LastCheckedPosition, transform.position) >= VRBConstants.ADJUSTED_METERS_FACTOR)
        {
            v3LastCheckedPosition = transform.position;
            fTotalMeters++;
            if (((GetComponent<PersistanceController>().settings.bDistanceDuration) && (fTotalMeters >= (VRBConstants.MAX_DURATION_DISTANCE[GetComponent<PersistanceController>().settings.iDifficulty]))) 
                || ((!GetComponent<PersistanceController>().settings.bDistanceDuration) && (fTimeTrainingSeconds >= VRBConstants.MAX_DURATION_TIME[GetComponent<PersistanceController>().settings.iDifficulty] )))
            {
                bPause = true;
                Debug.Log("fTimeTrainingSeconds: " + fTimeTrainingSeconds);
                GetComponent<MenuController>().StopTraining(fTotalMeters, fTimeTrainingSeconds);
            }
        }
        if (GetComponent<PersistanceController>().settings.bDistanceDuration)
        {
            fMetersReverse = VRBConstants.MAX_DURATION_DISTANCE[GetComponent<PersistanceController>().settings.iDifficulty] - fTotalMeters;
            fTimeReverse = fTimeTrainingSeconds;
        }
        else
        {
            fMetersReverse = fTotalMeters;
            fTimeReverse = VRBConstants.MAX_DURATION_TIME[GetComponent<PersistanceController>().settings.iDifficulty] - fTimeTrainingSeconds;
        }
        txtMeters.text = "Metros: " + fMetersReverse.ToString("F0");
        txtTime.text = "Tiempo: " + Mathf.Floor(fTimeReverse / 60).ToString("00") + ":" + (fTimeReverse % 60).ToString("00");
    }

    private float GetDifferenceBetweenGyros()
    {
        return Quaternion.Angle(qInitialGyro, qCurrentGyro);
    }

    private bool Increasing()
    {
        return (fGyroZ > fPrevGyroZ);
    }

    private bool Decreasing()
    {
        return (fGyroZ < fPrevGyroZ);
    }

    private void IncreasingOrDecreasing()
    {
        qPrevGyro = qCurrentGyro;
        bIncreasing = Increasing();
        bDecreasing = Decreasing();
    }

    private float CalculateSpeed()
    {
        if (Decreasing())
        {
            fFinalTime = Time.time;
            fFinalX = fGyroZ;
            GetComponent<Renderer>().material.color = Color.green;
            bDecreasing = true;
            bIncreasing = false;
        }
        else if (Increasing())
        {
            fInitialTime = Time.time;
            fInitialX = fGyroZ;
            GetComponent<Renderer>().material.color = Color.red;
            bIncreasing = true;
            bDecreasing = false;
        }
        fDistance = Mathf.Abs(fFinalX - fInitialX);
        fTime = Mathf.Abs(fFinalTime - fInitialTime);
        fPrevGyroZ = fGyroZ;
        return (fDistance / fTime)*5f;
    }

    private float GetSpeed()
    {
        return (fCurrentAngle * RADIUS) / (fFinalTime - fInitialTime);
    }

    private void InicializarVariables()
    {
        qInitialGyro = qCurrentGyro;
        fPreviousAngle = 0f;
        fInitialTime = Time.time;
    }

    public void OnEnable()
    {
        fPreviousTimeStamp = Time.realtimeSinceStartup;
    }
}
