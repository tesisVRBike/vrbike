﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

// Require a character controller to be attached to the same game object
[RequireComponent(typeof(CharacterMotor))]
[AddComponentMenu("Character/FPS Input Controller")]
[RequireComponent(typeof(AudioSource))]
public class FPSInputController : MonoBehaviour
{
	private CharacterMotor motor;
    private CardboardHead head;
	public bool checkAutoWalk = false;
    [SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
    [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
    [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.
    [SerializeField] private MouseLook m_MouseLook;

    private Camera m_Camera;
    private AudioSource m_AudioSource;
    private float m_StepCycle;
    private float m_NextStep;
    private CharacterController m_CharacterController;
    public bool m_Jumping;

    // Use this for initialization
    void Awake()
	{
		motor = GetComponent<CharacterMotor>();
        head = GetComponent<CardboardHead>();
    }
	
    private void Start()
    {
        m_CharacterController = GetComponent<CharacterController>();
        m_Camera = Camera.main;
        m_StepCycle = 0f;
        m_NextStep = m_StepCycle / 2f;
        m_AudioSource = GetComponent<AudioSource>();
        m_MouseLook.Init(transform, m_Camera.transform);
        m_Jumping = false;
        PlayLandingSound();
    }
	// Update is called once per frame
	void Update()
    {
        m_Jumping = false;
        //RotateView();
        // Get the input vector from keyboard or analog stick
        Vector3 directionVector;
		if (!checkAutoWalk) { 
			directionVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		} else { 
			directionVector = new Vector3(0, 0, 1);
		}

		if (directionVector != Vector3.zero)
		{
			// Get the length of the directon vector and then normalize it
			// Dividing by the length is cheaper than normalizing when we already have the length anyway
			float directionLength = directionVector.magnitude;
			directionVector = directionVector / directionLength;
			
			// Make sure the length is no bigger than 1
			directionLength = Mathf.Min(1.0f, directionLength);
			
			// Make the input vector more sensitive towards the extremes and less sensitive in the middle
			// This makes it easier to control slow speeds when using analog sticks
			directionLength = directionLength * directionLength;
			
			// Multiply the normalized direction vector by the modified length
			directionVector = directionVector * directionLength;
		}
		
		// Apply the direction to the CharacterMotor
		motor.inputMoveDirection = transform.rotation * directionVector;
        motor.inputJump = Input.GetButton("Jump");
        if (Input.GetButton("Jump") && !m_Jumping)
        {
            PlayJumpSound();
            m_Jumping = true;
        }
    }

    private void PlayLandingSound()
    {
        //m_AudioSource.clip = m_LandSound;
        //m_AudioSource.Play();
        m_NextStep = m_StepCycle + .5f;
    }

    private void PlayJumpSound()
    {
        //m_AudioSource.clip = m_JumpSound;
        //m_AudioSource.Play();
    }

    private void PlayFootStepAudio()
    {
        if (!m_CharacterController.isGrounded)
        {
            return;
        }
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, m_FootstepSounds.Length);
        //m_AudioSource.clip = m_FootstepSounds[n];
        //m_AudioSource.PlayOneShot(m_AudioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        m_FootstepSounds[n] = m_FootstepSounds[0];
        m_FootstepSounds[0] = m_AudioSource.clip;
    }
    
    private void RotateView()
    {
        m_MouseLook.LookRotation(transform, m_Camera.transform);
    }
}