﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRBConstants {
    public const string TAG_CANVAS = "Canvas";
    public const string TAG_HEAD = "Head";

    public const float TERRAIN_SIDE_LENGTH = 1000f;
    public const float TERRAIN_TO_REPOSITION = 1500f;

    public const float ACCELERATION_INPUT_LERP_SPEED = 2f;
    public const float SPEED_FACTOR = 20f;
    public const float SPPED_FACTOR_SENSIBILITY_MODIFIER = 10f;
    public const float MIN_ACCELERATION_COUNT = 0.01f;

    public const float MAX_TIME_BETWEEN_ACCELERATION = 0.25f;
#if UNITY_EDITOR
    public const float MAX_TIME_UNTIL_PAUSE = 2000f;
    public static readonly float[] MAX_DURATION_TIME = { 60f, 100f , 200f };
    public static readonly int[] MAX_DURATION_DISTANCE = { 100, 200, 300 };
#else
    public const float MAX_TIME_UNTIL_PAUSE = 5f;
    public static readonly float[] MAX_DURATION_TIME = { 600f, 1200f , 1800f };
    public static readonly int[] MAX_DURATION_DISTANCE = { 1000, 2000, 3000 };
#endif
    public const float ACCELEROMETER_FILTER_NOT_IN_X = 0.25f;

    public const float ADJUSTED_METERS_FACTOR = 10f;

    public const float GAZE_INPUT_TIME = 1.5f;

    public static readonly UnityEngine.Vector3 PAUSE_MENU_OFFSET = new UnityEngine.Vector3(0.25f,0f,5f);
    public static readonly UnityEngine.Vector3 INITIAL_POSITION = new UnityEngine.Vector3(0f, 7f, 0f);

    public const string KEY_SETTINGS = "key_settings";
    public const string KEY_TRAININGS = "key_trainings";

    public const int TABLE_TRAINING_TIME = 0, TABLE_TRAINING_METERS = 1, TABLE_TRAINING_DIFFICULTY = 2;
    public const int NUMBER_OF_SAVED_TRAININGS = 5;

    public const string LANG_DIFFICULTY_LOW     = "Baja";
    public const string LANG_DIFFICULTY_MID     = "Media";
    public const string LANG_DIFFICULTY_HIGH    = "Alta";
    public const string LANG_SCENARIO_FOREST    = "Escenario: Bosque";
    public const string LANG_SCENARIO_BEACH     = "Escenario: Playa";

    public const string LANG_SENSIBILITY        = "Sensibilidad: ";

    

}
