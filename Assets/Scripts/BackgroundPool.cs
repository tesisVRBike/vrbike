﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundPool : MonoBehaviour {
    public GameObject[] prefTrees;
    public GameObject[] prefTrees2;
    public GameObject[] prefTerrain;
    public GameObject[] prefTerrain2;

    private Vector2 v2TreesPerTerrain       = new Vector2(20, 45);
    private Vector2 v2TreesPositionXZ       = new Vector2(0, 100);
    private Vector3 v3StartTerrainPosition   = new Vector3(-VRBConstants.TERRAIN_SIDE_LENGTH,0, -VRBConstants.TERRAIN_SIDE_LENGTH);

    private GameObject[] goPoolTerrain;

    public Transform TerrainContainer;

    private int iNumberOfAxisTerrain = 3;

    private bool bPreparedTerrain = false;

    private int iSkipCheck = 25;
    private int iFrame = 0;
	// Use this for initialization
	void Start () {
        //FillPoolTerrain();

    }
	
	// Update is called once per frame
	void Update () {
        iFrame++;
        if (bPreparedTerrain && iFrame % iSkipCheck == 0)
        {
            CheckPosition();
        }

    }

    public void FillPoolTerrain(int iScenario)
    {
        if (TerrainContainer.childCount > 0)
        {

            for (int i = TerrainContainer.childCount - 1; i >= 0; i--)
            {
                GameObject.Destroy(TerrainContainer.GetChild(i).gameObject);
                Debug.Log("Destroyed");
            }
        }
        goPoolTerrain = new GameObject[iNumberOfAxisTerrain * iNumberOfAxisTerrain];
        //Instantiate Terrains
        for(int i = 0; i < iNumberOfAxisTerrain; i++)
        {
            for(int j = 0; j < iNumberOfAxisTerrain; j++)
            {
                GameObject goNewTerrain = Instantiate((iScenario == 0) ? prefTerrain[Random.Range(0, prefTerrain.Length)] : prefTerrain2[Random.Range(0, prefTerrain2.Length)], TerrainContainer) as GameObject;
                goNewTerrain.transform.localPosition = v3StartTerrainPosition + new Vector3(i* VRBConstants.TERRAIN_SIDE_LENGTH, 0, j * VRBConstants.TERRAIN_SIDE_LENGTH);
                //Instantiate Trees
                int iNumberOfTrees = (int)Random.Range(v2TreesPerTerrain.x, v2TreesPerTerrain.y);
                for (int z = 0; z < iNumberOfTrees; z++)
                {
                    GameObject goNewTree = Instantiate((iScenario == 0) ? prefTrees[Random.Range(0, prefTrees.Length)]: prefTrees2[Random.Range(0, prefTrees2.Length)], goNewTerrain.transform) as GameObject;
                    goNewTree.transform.localPosition = new Vector3(Random.Range(v2TreesPositionXZ.x, -v2TreesPositionXZ.y), 0, Random.Range(v2TreesPositionXZ.x, v2TreesPositionXZ.y));
                }
                goPoolTerrain[j + i * iNumberOfAxisTerrain] = goNewTerrain;
            }
        }
        bPreparedTerrain = true;
    }

    private void CheckPosition()
    {
        for (int i=0; i < goPoolTerrain.Length; i++ )
        {
            if (Mathf.Abs(transform.position.x - goPoolTerrain[i].transform.position.x) > VRBConstants.TERRAIN_TO_REPOSITION)
            {
                goPoolTerrain[i].transform.position = goPoolTerrain[i].transform.position + ((transform.position.x > goPoolTerrain[i].transform.position.x) ? Vector3.right: Vector3.left) * VRBConstants.TERRAIN_SIDE_LENGTH * iNumberOfAxisTerrain;
            }
            
            if (Mathf.Abs(transform.position.z - goPoolTerrain[i].transform.position.z) > VRBConstants.TERRAIN_TO_REPOSITION)
            {
                goPoolTerrain[i].transform.position = goPoolTerrain[i].transform.position + ((transform.position.z > goPoolTerrain[i].transform.position.z) ? Vector3.forward : Vector3.back) * VRBConstants.TERRAIN_SIDE_LENGTH * iNumberOfAxisTerrain;
            }
        }
    }

    public void InitTraining()
    {
        Debug.Log("toco boton");
    }
}
